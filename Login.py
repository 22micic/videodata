from AdminLogin import *
from report import *
from StartApp import *
import pymysql

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def login_admin():
    username = input("Enter Username: ")
    print("")
    password = input("Enter Password: ")
    query = "SELECT * FROM user WHERE username = '%s' AND password = '%s' AND roles = 'Admin'" % (username, password)

    try:
        with conn.cursor() as cur:
            # insert = (username, password)
            cur.execute(query)
            users = [row[1] for row in cur.fetchall()]
            if password in users:
                print(" ")
                print("You have logged in as an Admin.")
                print(" ")
                report_admin = Admin()
                report_admin.admin_choice(0)
                # choice_start(0)
            else:
                print("Login failed!")

    except Error as error:
        print("Login failed! ")
        print(error)

    # finally:
    #     conn.close()


def login_employee():
    username = input("Enter Username: ")
    password = input("Enter Password: ")
    query = "SELECT * FROM user WHERE username = '%s' AND password = '%s' AND roles = 'Employee'" % (username, password)

    try:
        with conn.cursor() as cur:
            # insert = (username, password)
            cur.execute(query)
            users = [row[1] for row in cur.fetchall()]
            if password in users:
                # if username in users and password in users:
                # users = cur.fetchall()
                print(" ")
                print("You have logged in as an Employee.")
                print(" ")
                choice_start(0)
            else:
                print("Login failed! ")

    except Error as error:
        print("Login failed! ")
        print(error)

        # finally:
        #     conn.close()


class LoginChoice:
    def choose_login(self, retry):
        enter = input("Would you like to log in, as Admin or as Employee? 1. Admin, 2. Employee: ")

        if retry == 1:
            print("Please enter a valid Choice: ")
            print("")

        if enter == "1":
            print("")
            login_admin()

        elif enter == "2":
            print("")
            login_employee()

        else:
            return self.choose_login(1)


login = LoginChoice()
login.choose_login(0)
