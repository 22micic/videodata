from DeleteCustomer import *
from DeleteMovie import *
from DeleteRental import *


def choice_delete(retry):
    print("Would you like to: 1. Delete a Movie, 2: Delete a Customer or 3. Delete Rental?")
    choose = input("Please enter: ")

    if retry == 1:
        print("Please enter a valid Choice:")

    if choose == "1":
        print("")
        delete = Movie()
        delete.delete_movie()
    elif choose == "2":
        print("")
        delete = Customer()
        delete.delete_customer()
    elif choose == "3":
        print("")
        delete = Rental()
        delete.delete_rental()
    else:
        return choice_delete(1)


# choice_delete(0)


