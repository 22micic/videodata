import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def movie_again(retry):
    choose = input("Would you like to insert another Movie? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Movie()
        run.main()
    elif choose == "2":
        print("Closing insert Movie...")
        conn.close()
    else:
        return movie_again(1)


class Movie:
    def main(self):
        print("Enter a new Movie")
        print(" ")

        movie_title = input("Enter the movie title: ")

        while True:
            try:
                release_year = int(input("Enter the release year of the movie: "))
            except ValueError:
                print("Enter a year: ")
                continue
            else:
                break

        movie_description = input("Enter the movie description ")

        while True:
            try:
                in_stock = int(input("In stock?: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        while True:
            rental_or_sale_or_both = input("For rent/buy/both? Enter status: ")
            if rental_or_sale_or_both == "rent":
                break
            elif rental_or_sale_or_both == "buy":
                break
            elif rental_or_sale_or_both == "both":
                break
        # Not pretty but i works

        while True:
            try:
                rental_daily_rate = float(input("Daily rate: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        while True:
            try:
                sale_price = float(input("Movie price: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        print(" ")

        sql = "INSERT INTO movies (`movie_title`, `release_year`, `movie_description`, `in_stock`, " \
              "`rental_or_sale_or_both`, `rental_daily_rate`, `sale_price`) " \
              "VALUES (%s, '%s', %s, '%s', %s, '%s', '%s')"

        try:
            with conn.cursor() as cursor:
                insert_data = (movie_title, release_year, movie_description, in_stock, rental_or_sale_or_both,
                               rental_daily_rate, sale_price)
                cursor.execute(sql, insert_data)
                print("You have successfully added a Movie to the library.")
                conn.commit()
                movie_again(0)

        except Error as e:
                print(e)
                conn.commit()

        # finally:
        #     conn.close()


# run = Movie()
# run.main()

#
# test = input("Enter a new Movie?: ")
# print(type(test))
# if type(test) == str:
#     print(type(test))
