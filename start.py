# import sqlite3

from Movies import *
from Customer import *
from Rental import *


# Customer 101, Movies 201, rent/buy/both


def choice(retry):
    print("Would you like to: 1. Add Movie, 2: Add a Customer or 3. Rent a movie?")
    choose = int(input("Please enter: "))

    if retry == 1:
        print("Please enter a valid Choice:")

    if choose == 1:
        movie_run = Movie()
        movie_run.insert_movie()
    elif choose == 2:
        customer_run = Customer()
        customer_run.insert_customer()
    elif choose == 3:
        rent_run = Rental()
        rent_run.insert_rental()
    else:
        return choice(1)


choice(0)
