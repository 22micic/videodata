from Movies import *
from Customer import *
from Rental import *


# Customer 101, Movies 201, rent/buy/both

def choice_insert(retry):
    print("Would you like to: 1. Add Movie, 2: Add a Customer or 3. Rent/Buy a movie?")
    choose = input("Please enter: ")

    if retry == 1:
        print("Please enter a valid Choice:")

    if choose == "1":
        print("")
        movie = Movie()
        movie.main()
    elif choose == "2":
        print("")
        customer = Customer()
        customer.main()
    elif choose == "3":

        rental = Rental()
        rental.main()
    else:
        return choice_insert(1)


# choice_insert(0)
