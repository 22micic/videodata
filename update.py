from UpdateMovie import *
from UpdateCustomer import *
from UpdateRental import *


# Customer 101, Movies 201, rent/buy/both


def choice_update(retry):
    print("Would you like to: 1. Update a Movie, 2: Update a Customer or 3. Update Rental?")
    choose = input("Please enter: ")

    if retry == 1:
        print("Please enter a valid Choice:")

    if choose == "1":
        print("")
        update_movie = MovieUpdate()
        update_movie.update_movie(0)
    elif choose == "2":
        print("")
        update_customer = CustomerUpdate()
        update_customer.update_customer(0)
    elif choose == "3":
        print("")
        update_rental = RentalUpdate()
        update_rental.update_rental(0)
    else:
        return choice_update(1)


# choice_update(0)
