import pymysql
from pymysql import Error


conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def insert_again(retry):
    choose = input("Would you like to insert another customer? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Customer()
        run.main()
    elif choose == "2":
        print("Closing insert customer...")
        conn.close()
    else:
        return insert_again(1)


class Customer:
    def main(self):
        print("Enter a new Customer")
        print(" ")

        customer_first_name = input("Enter the first name of the Customer: ")
        customer_last_name = input("Enter the last name of the Customer: ")
        customer_address = input("Enter the address of the Customer: ")
        customer_phone = input("Enter the phone of the Customer: ")
        customer_email = input("Enter the email of the customer: ")

        print(" ")

        sql = "INSERT INTO customer (customer_first_name, customer_last_name, " \
              "customer_address, customer_phone, customer_email) " \
              "VALUES (%s, %s, %s, %s, %s)"

        try:
            with conn.cursor() as cursor:
                insert_data = (customer_first_name, customer_last_name, customer_address,
                               customer_phone, customer_email)
                cursor.execute(sql, insert_data)
                print("You have successfully added a Customer to the database.")
                conn.commit()

                insert_again(0)

        except Error as e:
                print(e)
                conn.commit()
        # finally:
        #     conn.close()


# run = Customer()
# run.main()
