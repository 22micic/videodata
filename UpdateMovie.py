import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def update_again(retry):
    choose = input("Would you like to update another movie? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = MovieUpdate()
        run.update_movie(0)

    elif choose == "2":
        print("Closing update movie...")
        conn.close()

    else:
        return update_again(1)


class MovieUpdate:
    def update_movie(self, retry):
        if retry == 1:
            print("Please enter a valid Choice:")
            print(" ")

        print("Update Movie from database: ")
        choose = input("Which do you want to Change/Update 1. Movie Title, 2. Release year,. 3. Movie Description "
                       "4. in_stock, 5. Rent/Sale/Both 6. Daily rate, 7. Sale price ?: ")
        print(" ")

        if choose == "1":
            movie_id = input("Enter movie id: ")
            movie_title = input("To what title do you wish to change? ")

            print(" ")

            # UPDATE `data`.`movies` SET `movie_title` = 'Rider' WHERE (`movie_id` = '206');

            query = "UPDATE `movies` SET `movie_title` = '%s' WHERE (`movie_id` = '%s')" % \
                    (movie_title, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    conn.commit()
                    print("You have successfully updated the Movie title.")

                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "2":
            movie_id = input("Enter movie id: ")
            release_year = input("To what year do you wish to change? ")

            print(" ")

            query = "UPDATE `movies` SET `release_year` = '%s' WHERE (`movie_id` = '%s')" % \
                    (release_year, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie release year.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "3":
            movie_id = input("Enter movie id: ")
            movie_description = input("Change description:  ")

            print(" ")

            query = "UPDATE `movies` SET `movie_description` = '%s' WHERE (`movie_id` = '%s')" % \
                    (movie_description, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie description.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "4":
            movie_id = input("Enter movie id: ")
            in_stock = input("How many in stock are there? ")

            print(" ")

            query = "UPDATE `movies` SET `in_stock` = '%s' WHERE (`movie_id` = '%s')" % \
                    (in_stock, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated how many Movies are in stock.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "5":
            movie_id = input("Enter movie id: ")
            rental_or_sale_or_both = input("For rent, sale, or both? ")

            print(" ")

            query = "UPDATE `movies` SET `rental_or_sale_or_both` = '%s' WHERE (`movie_id` = '%s')" % \
                    (rental_or_sale_or_both, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie rent/buy/both status.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "6":
            movie_id = input("Enter movie id: ")
            rental_daily_rate = input("What's the daily rental rate? ")

            print(" ")

            query = "UPDATE `movies` SET `rental_daily_rate` = '%s' WHERE (`movie_id` = '%s')" % \
                    (rental_daily_rate, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie daily rent rate.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "7":
            movie_id = input("Enter movie id: ")
            sale_price = input("What's the new price? ")

            print(" ")

            query = "UPDATE `movies` SET `sale_price` = '%s' WHERE (`movie_id` = '%s')" % \
                    (sale_price, movie_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie sale price.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        else:
            return self.update_movie(1)


# movie = MovieUpdate()
# movie.update_movie(0)
