import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def insert_again(retry):
    choose = input("Would you like to insert another purchase? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Rental()
        run.main()
    elif choose == "2":
        print("Closing insert purchase...")
        conn.close()
    else:
        return insert_again(1)


class Rental:
    def main(self):
        print("Insert a new Purchase")
        print(" ")

        customer_id = input("Enter customer ID: ")
        movie_id = input("Enter the movie ID: ")
        status_id = input("Enter status: ")
        rental_date_out = input("Enter rental date: ")
        rental_date_returned = input("Enter the return date: ")
        rental_amount_due = input("Enter the daily rent rate: ")
        sale_price = input("Enter sales price: ")

        print(" ")

        sql = "INSERT INTO rental (customer_id, movie_id, status_id, rental_date_out, " \
              "rental_date_returned, rental_amount_due, sale_price) " \
              "VALUES (%s, %s, %s, %s, %s, %s, %s)"

        try:
            with conn.cursor() as cursor:
                insert_data = (customer_id, movie_id, status_id, rental_date_out, rental_date_returned,
                               rental_amount_due, sale_price)
                cursor.execute(sql, insert_data)
                print("Purchase was successful")
                conn.commit()
                insert_again(0)
        except Error as e:
                print("Error found in application", e)

        finally:
            conn.close()


# run = Rental()
# run.main()
