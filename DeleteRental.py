import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def delete_again(retry):
    choose = input("Would you like to delete another customer? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Rental()
        run.delete_rental()
    elif choose == "2":
        print("Closing delete customer...")
        conn.close()
    else:
        return delete_again(1)


class Rental:
    def delete_rental(self):
        print("Delete Rental from database: ")
        print(" ")

        movie_id = input("Enter movie id: ")

        print(" ")

        query = "DELETE FROM rental WHERE rental_id = %s"

        try:
            with conn.cursor() as cursor:
                cursor.execute(query, (movie_id,))
                print("The Rental has been successfully deleted from the database.")
                conn.commit()
                delete_again(0)

        except Error as error:
            print(error)

        # finally:
        #     conn.close()


# rental = Rental()
# rental.delete_rental()
