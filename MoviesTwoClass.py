import pymysql
from pymysql import Error





class InsertMovie:
    def check_connection(self, db_file):
        try:
            conn = pymysql.connect(db_file)

            return conn
        except Error as e:
            print(e)

        return None

    def insert_movie(self, conn, insert):
        conn = pymysql.connect(user='root', password='groot',
                               host='localhost',
                               database='VideoClubData',
                               port='3306')
        cur = conn.cursor()
        sql = "INSERT INTO Movies (movie_title, release_year, movie_description, in_stock, " \
              "rental_or_sale_or_both, rental_daily_rate, sale_price) VALUES (%s, %s, %s, %s, %s, %s)"
        try:
            cur.execute(sql, insert)
            print("Inserted into db")
            print("")
            return cur.lastrowid
        except Error as error:

            print(error)

            # print("Error was found:::: ", error)

        # return None

    def main_insertmovie(self):
        print("Enter a new Movie")
        print(" ")
        #
        # test = input("Enter a new Movie?: ")
        # print(type(test))
        # if type(test) == str:
        #     print(type(test))

        movie_title = input("Enter the movie title: ")

        # release_year = int(input("Enter the release year of the movie: "))
        while True:
            try:
                release_year = int(input("Enter the release year of the movie: "))
            except ValueError:
                print("Enter a year: ")
                continue
            else:
                break

        movie_description = input("Enter the movie description ")

        while True:
            try:
                in_stock = int(input("In stock?: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        rental_or_sale_or_both = input("For rent/buy/both? Enter status: ")
        # while True:
        #     rental_or_sale_or_both = input("For rent/buy/both? Enter status: ")
        #     if rental_or_sale_or_both == "rent":
        #         break
        #     elif rental_or_sale_or_both == "buy":
        #         break
        #     elif rental_or_sale_or_both == "both":
        #         break
        # Not pretty but i works

        # rental_daily_rate = float(input("Daily rate: "))
        while True:
            try:
                rental_daily_rate = float(input("Daily rate: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        # sale_price = float(input("Movie price: "))
        while True:
            try:
                sale_price = float(input("Movie price: "))
            except ValueError:
                print("Enter a number: ")
                continue
            else:
                break

        print(" ")

        database = "C:\\Users\MSG_CC1\Desktop\Python\VideoData/videoclubdata.db"

        conn = self.check_connection(database)
        with conn:
            task_1 = (movie_title, release_year, movie_description, in_stock, rental_or_sale_or_both,
                      rental_daily_rate, sale_price)

            self.insert_movie(conn, task_1)


run_method = InsertMovie()
run_method.main_insertmovie()
