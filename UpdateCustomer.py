import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def update_again(retry):
    choose = input("Would you like to update another customer? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = CustomerUpdate()
        run.update_customer(0)

    elif choose == "2":
        print("Closing update customer...")
        conn.close()

    else:
        return update_again(1)


class CustomerUpdate:
    def update_customer(self, retry):
        if retry == 1:
            print("Please enter a valid Choice:")
            print(" ")

        print("Update Customer from database: ")
        choose = input("Which do you want to Change/Update 1. First name, 2. Last name, 3. Address, 4. Phone  "
                       "5. E - mail ?: ")
        print(" ")

        if choose == "1":
            customer_id = input("Enter customer id: ")
            customer_first_name = input("To what name do you want to change? ")

            print(" ")

            query = "UPDATE `customer` SET `customer_first_name` = '%s' WHERE (`customer_id` = '%s')" % \
                    (customer_first_name, customer_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customers first name.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "2":
            customer_id = input("Enter customer id: ")
            customer_last_name = input("To what last name do you want to change? ")

            print(" ")

            query = "UPDATE `customer` SET `customer_last_name` = '%s' WHERE (`customer_id` = '%s')" % \
                    (customer_last_name, customer_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customers Last name.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "3":
            customer_id = input("Enter customer id: ")
            customer_address = input("What's the customers address?:  ")

            print(" ")

            query = "UPDATE `customer` SET `customer_address` = '%s' WHERE (`customer_id` = '%s')" % \
                    (customer_address, customer_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customers address.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "4":
            customer_id = input("Enter customer id: ")
            customer_phone = input("To What phone number do you want to change? ")

            print(" ")

            query = "UPDATE `customer` SET `customer_phone` = '%s' WHERE (`customer_id` = '%s')" % \
                    (customer_phone, customer_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customers phone number")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "5":
            customer_id = input("Enter customer id: ")
            customer_email = input("To what e-mail do you want to change? ")

            print(" ")

            query = "UPDATE `customer` SET `customer_email` = '%s' WHERE (`customer_id` = '%s')" % \
                    (customer_email, customer_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customers e-mail")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        else:
            return self.update_customer(1)


# update_customer = CustomerUpdate()
# update_customer.update_customer(0)
