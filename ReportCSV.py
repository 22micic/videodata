from StartApp import *
import pymysql
import csv

db = pymysql.connect(host='127.0.0.1',
                     user='root',
                     password='1234',
                     db='data',
                     port=3306)


def movie_report():
    query = 'SELECT * FROM movies'

    cur = db.cursor()
    cur.execute(query)
    result = cur.fetchall()

    c = csv.writer(open('MoviesReport01.csv', 'w'))
    for x in result:
        c.writerow(x)


def customer_report():
    query = 'SELECT * FROM customer'

    cur = db.cursor()
    cur.execute(query)
    result = cur.fetchall()

    c = csv.writer(open('CustomerReport01.csv', 'w'))
    for x in result:
        c.writerow(x)


def rental_report():
    query = 'SELECT * FROM rental'

    cur = db.cursor()
    cur.execute(query)
    result = cur.fetchall()

    c = csv.writer(open('RentalReport01.csv', 'w'))
    for x in result:
        c.writerow(x)


def startapp():
    try:
        print("Going to the App...")
        print("")
        choice_start(0)

    except Error as e:
        print(e)


def again_back(retry):
    again = input("Would you like to see another report or continue to the App? 1. Another Sales report "
                  "2. Another Rent report 3. Start App: ")
    print("")
    if retry == "1":
        print("Please enter a valid choice")

    if again == "1":
        print("Export another report..")
        print("")
        reports = ChoiceReport()
        reports.choose(0)

    elif again == "2":
        print("Another rent report..")
        print("")
        startapp()

    else:
        return again_back(1)


class ChoiceReport:
    def choose(self, retry):
        report = input("Which table report do you want to export? 1. Customer. 2. Movies. 3. Rental:")
        print("")

        if retry == "1":
            print("Please enter a valid choice")

        if report == "1":
            print("Exporting customer report..")
            print("")
            customer_report()

        elif report == "2":
            print("Exporting Movies report..")
            print("")
            movie_report()

        elif report == "3":
            print("Exporting rental report..")
            print("")
            rental_report()

        else:
            return self.choose(1)


# reports = ChoiceReport()
# reports.choose(0)
