import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_movie(conn, task):
    print('Inserted into db')

    sql = '''INSERT INTO Movies (movie_title, release_year, movie_description, in_stock,
             rental_or_sale_or_both, rental_daily_rate, sale_price)
             VALUES (?, ?, ?, ?, ?, ?, ?)'''
    cur = conn.cursor()
    cur.execute(sql, task)
    conn.commit()
    return cur.lastrowid


def main_movie():
    print("Enter a new Movie")
    print(" ")

    movie_title = input("Enter the movie title: ")
    release_year = int(input("Enter the release year of the movie: "))
    movie_description = input("Enter the movie desctiption ")
    in_stock = int(input("In stock?: "))
    rental_or_sale_or_both = input("For rent/buy/both? Enter status: ")
    rental_daily_rate = float(input("Daily rate: "))
    sale_price = float(input("Movie price: "))

    print(" ")
    print("Inserted into db")

    database = "C:\\Users\MSG_CC1\Desktop\Python\VideoData/newdb.db"

    conn = create_connection(database)

    with conn:
        task_1 = (movie_title, release_year, movie_description, in_stock, rental_or_sale_or_both,
                  rental_daily_rate, sale_price)

        create_movie(conn, task_1)


main_movie()

# customer = main_movie()
# customer.main_movie()
