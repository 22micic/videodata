import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def update_again(retry):
    choose = input("Would you like to delete another purchase? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = RentalUpdate()
        run.update_rental(0)

    elif choose == "2":
        print("Closing update purchase...")
        conn.close()

    else:
        return update_again(1)


class RentalUpdate:
    def update_rental(self, retry):
        if retry == 1:
            print("Please enter a valid Choice:")
            print(" ")

        print("Update Rental from database: ")
        choose = input("Which do you want to Change/Update 1. Customer id, 2. Movie id, 3. Rent/Buy/Both, "
                       "4. Rental date out 5. Rental date returned, 6. Rental amount due, 7. Sales price ?: ")
        print(" ")

        if choose == "1":
            rental_id = input("Enter Rental id: ")
            customer_id = input("Change customer id: ")

            print(" ")

            query = "UPDATE `rental` SET `customer_id` = '%s' WHERE (`rental_id` = '%s')" % \
                    (customer_id, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Customer id.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "2":
            rental_id = input("Enter Rental id: ")
            movie_id = input("Change movie id: ")

            print(" ")

            query = "UPDATE `rental` SET `movie_id` = '%s' WHERE (`rental_id` = '%s')" % \
                    (movie_id, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the Movie id.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "3":
            rental_id = input("Enter Rental id: ")
            status_id = input("Change rent/buy/both:  ")

            print(" ")

            query = "UPDATE `rental` SET `status_id` = '%s' WHERE (`rental_id` = '%s')" % \
                    (status_id, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the rent/buy/both status id.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "4":
            rental_id = input("Enter Rental id: ")
            rental_date_out = input("Change rental date out to: ")

            print(" ")

            query = "UPDATE `rental` SET `rental_date_out` = '%s' WHERE (`rental_id` = '%s')" % \
                    (rental_date_out, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the rental date out.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "5":
            rental_id = input("Enter Rental id: ")
            rental_date_returned = input("Change rental date return to: ")

            print(" ")

            query = "UPDATE `rental` SET `rental_date_returned` = '%s' WHERE (`rental_id` = '%s')" % \
                    (rental_date_returned, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated rental date returned.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "6":
            rental_id = input("Enter Rental id: ")
            rental_amount_due = input("Change amount due to: ")

            print(" ")

            query = "UPDATE `rental` SET `rental_amount_due` = '%s' WHERE (`rental_id` = '%s')" % \
                    (rental_amount_due, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated rental amount due.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        elif choose == "7":
            rental_id = input("Enter Rental id: ")
            sale_price = input("Change sales price to: ")

            print(" ")

            query = "UPDATE `rental` SET `sale_price` = '%s' WHERE (`rental_id` = '%s')" % \
                    (sale_price, rental_id)
            try:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    print("You have successfully updated the sale price.")
                    conn.commit()
                    update_again(0)

            except Error as error:
                print(error)

            # finally:
            #     conn.close()

        else:
            return self.update_rental(1)


# update_rental = RentalUpdate()
# update_rental.update_rental(0)
