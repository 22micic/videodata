from report import *
from ReportCSV import *
from ReportCustomer import *
from ReportMovies import *


def read_report(retry):
    try:
        print("Going to Read report...")
        print("")
        send = Report()
        send.choice_admin(0)

    except Error as e:
        print(e)


def export_report():
    try:
        print("Going to Export report...")
        print("")
        reports = ChoiceReport()
        reports.choose(0)

    except Error as e:
        print(e)


def startapp():
    try:
        print("Going to the App...")
        print("")
        choice_start(0)

    except Error as e:
        print(e)


class Admin:
    def admin_choice(self, retry):
        enter = input("Would you like to Read report or Export report or go to Start App? "
                      "1. Read report 2. Export Report: ")

        if retry == 1:
            print("Please enter a valid Choice: ")
            print("")

        if enter == "1":
            print("")
            read_report(0)

        elif enter == "2":
            print("")
            export_report()
        elif enter == "3":
            print("")
            startapp()

        else:
            return self.admin_choice(1)


# report_admin = Admin()
# report_admin.admin_choice(0)
