from StartApp import *
import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def again_back(retry):
    again = input("Would you like to see another report or continue to the App? 1. Another Sales report "
                  "2. Another Rent report 3. Start App: ")
    print("")
    if retry == "1":
        print("Please enter a valid choice")

    if again == "1":
        print("Another sales report..")
        print("")
        sales_report()

    elif again == "2":
        print("Another rent report..")
        print("")
        rent_report()
    elif again == "3":
        print("")
        startapp()
    else:
        return again_back(1)


def sales_report():
    report = input("Enter Customer id which you want to check: ")

    query = "SELECT SUM(sale_price) FROM rental Where customer_id = '%s'" % report

    try:
        c = conn.cursor()
        c.execute(query)
        result = c.fetchone()
        print("Total sum from the sales is", result, ". ")
        print("")
        again_back(0)

    except Error as e:
        print(e)


def rent_report():
    report = input("Enter Customer id which you want to check: ")

    query = "SELECT SUM(rental_amount_due) FROM rental Where customer_id = '%s'" % report

    try:
        c = conn.cursor()
        c.execute(query)
        result = c.fetchall()
        print("Total sum from the rent is", result, ". ")
        print("")
        again_back(0)

    except Error as e:
        print(e)


def startapp():
    try:
        print("Going to the App...")
        print("")
        choice_start(0)

    except Error as e:
        print(e)


class CustomerReport:
    def report_admin(self, retry):
        report_cus = input("Which report do you need? 1. Sales report, 2. Rent report: ")
        print("")

        if retry == "1":
            print("Please enter a valid choice")

        if report_cus == "1":
            print("Sales report")
            print("")
            sales_report()
        elif report_cus == "2":
            print("Rent report")
            print("")
            rent_report()
        else:
            return self.report_admin(1)


# report = CustomerReport()
# report.report_admin(0)
