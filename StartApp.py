from insert import *
from delete import *
from update import *
from read import *


def choice_start(retry):
    print("Would you like to: 1. Insert, 2: Update, 3. Read or 4. Delete? ")
    print(" Log out or Clock out? 5. Log out 6. Clock out")
    choose = input("Please enter: ")

    if retry == 1:
        print("Please enter a valid Choice:")

    if choose == "1":
        choice_insert(0)
    elif choose == "2":
        print("")
        choice_update(0)
    elif choose == "3":
        print("")
        read_all(0)
    elif choose == "4":
        print("")
        choice_delete(0)
    elif choose == "5":
        print("")
        print("Log out")
        conn.close()
    elif choose == "6":
        print("")
        print("Clock out")
        conn.close()
    else:
        return choice_start(1)


choice_start(0)
