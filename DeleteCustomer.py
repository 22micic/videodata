import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def delete_again(retry):
    choose = input("Would you like to delete another customer? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Customer()
        run.delete_customer()
    elif choose == "2":
        print("Closing delete customer...")
        conn.close()
    else:
        return delete_again(1)


class Customer:
    def delete_customer(self):
        print("Delete Customer from database: ")
        print(" ")

        customer_id = input("Enter customer id: ")

        print(" ")

        query = "DELETE FROM Customer WHERE customer_id = %s"

        try:
            with conn.cursor() as cursor:
                # execute the query
                # cursor = conn.cursor()
                cursor.execute(query, (customer_id,))
                print("The Customer has been successfully deleted from the database.")
                conn.commit()
                delete_again(0)

        except Error as error:
            print(error)

        # finally:
        #     conn.close()


# customer = Customer()
# customer.delete_customer()
