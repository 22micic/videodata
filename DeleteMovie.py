import pymysql
from pymysql import Error

conn = pymysql.connect(host='127.0.0.1',
                       user='root',
                       password='1234',
                       db='data',
                       port=3306)


def delete_again(retry):
    choose = input("Would you like to delete another movie? 1. Yes or 2. No ")
    if retry == 1:
        print("Please enter a valid Choice: ")

    if choose == "1":
        run = Movie()
        run.delete_movie()
    elif choose == "2":
        print("Closing delete movie...")
        conn.close()
    else:
        return delete_again(1)


class Movie:
    def delete_movie(self):
        print("Delete Movies from database: ")
        print(" ")

        movie_id = input("Enter movie id: ")

        print(" ")

        query = "DELETE FROM movies WHERE movie_id = %s"

        try:
            with conn.cursor() as cursor:
                # execute the query
                # cursor = conn.cursor()
                cursor.execute(query, (movie_id,))
                print("The Movie has been successfully deleted from the library.")
                conn.commit()
                delete_again(0)

        except Error as error:
            print(error)

        # finally:
        #     conn.close()


# movie = Movie()
# movie.delete_movie()

