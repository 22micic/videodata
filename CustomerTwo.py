import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_customer(conn, task):
    print('Inserted into db')

    sql = '''INSERT INTO Customer (customer_first_name, customer_last_name, customer_address, customer_phone, 
             customer_email) VALUES (?, ?, ?, ?, ?) '''
    cur = conn.cursor()
    cur.execute(sql, task)
    conn.commit()
    return cur.lastrowid


def main_customer():
    print("Enter a new customer: ")
    print(" ")

    customer_first_name = input("Enter the first name of the Customer: ")
    customer_last_name = input("Enter the last name of the Customer: ")
    customer_address = input("Enter the address of the Customer: ")
    customer_phone = input("Enter the phone of the Customer: ")
    customer_email = input("Enter the email of the customer: ")

    print(" ")
    print("Inserted into db")

    database = "C:\\Users\MSG_CC1\Desktop\Python\VideoData/newdb.db"

    conn = create_connection(database)

    with conn:
        task_1 = (customer_first_name, customer_last_name, customer_address, customer_phone, customer_email)

        create_customer(conn, task_1)


# customer = main_customer()
# customer.main_customer()


# import sqlite3
#
# conn = sqlite3.connect("newdb.db")
#
#
#
#
# class Customer:
#     def insert_customer(self):
#         print("Enter a new customer: ")
#         customer_first_name = input("Enter the first name of the Customer: ")
#         customer_last_name = input("Enter the last name of the Customer: ")
#         customer_address = input("Enter the address of the Customer: ")
#         customer_phone = input("Enter the phone of the Customer: ")
#         customer_email = input("Enter the email of the customer: ")
#
#         print("Inserted into db")
#
#         c = conn.cursor()
#         c.execute("INSERT INTO Customer (customer_first_name, customer_last_name, customer_address, customer_phone,"
#                   "customer_email)"
#                   " VALUES (?, ?, ?, ?, ?)",
#                   (customer_first_name, customer_last_name, customer_address, customer_phone, customer_email))
#
#         conn.commit()
#
#
# # customer_run = Customer()
# # customer_run.insert_customer()
#
# # Works
